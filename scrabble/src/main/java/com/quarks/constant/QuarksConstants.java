package com.quarks.constant;

/**
 * File to maintain constants
 * @author Vipin Kumar Vindal
 *
 */
public class QuarksConstants {
	
	/**
	 * Url path to serve the files
	 */
	public static final String FILESERVER_PATH = "/content-fileserver?fileId=";
	
	/**
	 * Property which specify where the filestore is located
	 */
	public static final String NDATA_SYSTEM_PATH_PROPERTY = "nDataPath";
	
	/**
	 * System Configurations
	 */
	public static final String QUARKS_HOST = "quarksHost";

    /**
     * temp location
     */
    public static final String TEMP_LOCATION = "/tmp";

    /**
     * number player to show in leader board
     */
    public static final Integer LEADER_BOARD_LIMIT = 10;

    /**
     * min number of match played to show in leader board
     */
    public static final Long LEADER_BOARD_MIN_MATCH_PLAYED = 10L;

}
