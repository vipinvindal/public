package com.quarks.exception;

/**
 * This exception should throw when Identity not valid
 * @author Ravinder Rathi
 */
public class InValidEncryptedIdException extends Exception {

	private static final long serialVersionUID = 4358827314361701072L;

	public InValidEncryptedIdException() {
    }

    public InValidEncryptedIdException(String message) {
        super(message);
    }
}
