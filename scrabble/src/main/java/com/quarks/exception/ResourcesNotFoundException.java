package com.quarks.exception;

/**
 * This Exception throw when http response should be 404
 * @author Ravinder Rathi
 */
public class ResourcesNotFoundException extends Exception {

	private static final long serialVersionUID = 8292429499813140455L;

	public ResourcesNotFoundException() {
    }

    public ResourcesNotFoundException(String message) {
        super(message);
    }
}
