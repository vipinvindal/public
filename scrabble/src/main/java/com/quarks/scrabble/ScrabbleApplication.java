package com.quarks.scrabble;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = { ErrorMvcAutoConfiguration.class })
@ComponentScan("com.quarks.scrabble")
public class ScrabbleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScrabbleApplication.class, args);
	}
}
