package com.quarks.scrabble.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.exception.ResourcesNotFoundException;

/**
 * Advice controller
 * Created by apple on 2/26/17.
 */
@ControllerAdvice
public class AdviceController {

    private static final Logger logger = LoggerFactory.getLogger(AdviceController.class);

    /**
     * Handling for 404
     */
    @ResponseStatus(value= HttpStatus.NOT_FOUND)
    @ExceptionHandler({ResourcesNotFoundException.class})
    public void handleNotFound(){
    	//returning 404 error code
        logger.debug("handler executed : ");
    }
    
    /**
     * Handling for 400
     */
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    @ExceptionHandler({InValidEncryptedIdException.class})
    public void handleBadrequest(){
    	//returning 400 error code
        logger.debug("handler executed : ");
    }
    
}
