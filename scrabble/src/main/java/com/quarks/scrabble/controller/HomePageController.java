package com.quarks.scrabble.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Home page controller
 * @author Ravinder Rathi
 */
@Controller("/")
public class HomePageController {

    private static final Logger logger = LoggerFactory.getLogger(HomePageController.class);

    /**
     * Mappings for the home page
     */
    @GetMapping({"home","index","/"})
    public String home(){
        logger.info("in home of HomePageController");
        return "site.homepage";
    }

}
