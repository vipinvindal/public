package com.quarks.scrabble.controller;

import com.quarks.scrabble.model.PlayerStats;
import com.quarks.scrabble.service.PlayerStatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * LeaderBoard Controller
 * @author Ravinder Rathi
 */
@Controller
public class LeaderBoardController extends ScrabbleController {

    private static final Logger logger = LoggerFactory.getLogger(LeaderBoardController.class);

    @Autowired
    PlayerStatsService playerStatsService;

    /**
     * Get leaderboard
     */
    @GetMapping("/leader-board")
    public String getLeaderBoard(Model model){
        logger.info("in getLeaderBoard of LeaderBoardController");
        List<PlayerStats> leaderBoard = playerStatsService.getLeaderBoard();
        model.addAttribute("leaderBoard",leaderBoard);
        return "site.leaderBoard";
    }
}
