package com.quarks.scrabble.controller;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.exception.ResourcesNotFoundException;
import com.quarks.scrabble.model.PlayerStats;
import com.quarks.scrabble.service.PlayerStatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Controller for player stats
 * @author Ravinder Rathi
 */
@Controller
public class PlayerStatsController extends ScrabbleController {

    private static final Logger logger = LoggerFactory.getLogger(PlayerStatsController.class);

    @Autowired
    PlayerStatsService playerStatsService;

    /**
     * get stats of a player
     * @param playerIdStr
     * @throws InValidEncryptedIdException
     * @throws ResourcesNotFoundException
     */
    @GetMapping("/stats/{playerIdStr}/")
    public String getPlayerStats(@PathVariable(value = "playerIdStr") String playerIdStr,Model model) throws InValidEncryptedIdException, ResourcesNotFoundException {
        logger.info("in getPlayerStats of PlayerStatsController");
        PlayerStats playerStats = playerStatsService.getPlayerStats(playerIdStr);
        if(playerStats == null){
            throw new ResourcesNotFoundException();
        }
        model.addAttribute("playerStats", playerStats);
        return "site.playerStats";
    }
}
