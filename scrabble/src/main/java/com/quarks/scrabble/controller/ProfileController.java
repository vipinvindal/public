package com.quarks.scrabble.controller;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.model.Player;
import com.quarks.scrabble.service.PlayerService;
import com.quarks.scrabble.validator.player.PlayerValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 * Profile controller for update user profile
 * Created by apple on 2/25/17.
 */
@Controller
@SessionAttributes("player")
public class ProfileController extends ScrabbleController{

    private static final Logger logger = LoggerFactory.getLogger(ScrabbleController.class);

    @Autowired
    PlayerService playerService;

    @Autowired
    PlayerValidator playerValidator;

    /**
     * Get Player profile
     * @param playerIdStr
     * @param model
     * @throws InValidEncryptedIdException
     */
    @GetMapping("/player/{playerIdStr}/")
    public String getPlayer(@PathVariable(value = "playerIdStr") String playerIdStr,Model model) throws InValidEncryptedIdException {
        logger.info("in getPlayer of ProfileController");
        Player player = playerService.getPlayer(playerIdStr);
        model.addAttribute("player",player);
        return "site.profile";
    }

    /**
     * Updates player
     * @param playerIdStr
     * @param player
     * @param model
     * @param bindingResult
     * @param status
     * @throws InValidEncryptedIdException
     */
    @PostMapping("/player/{playerIdStr}/")
    public String updatePlayer(@PathVariable(value = "playerIdStr") String playerIdStr,@ModelAttribute("player")Player player,
    								Model model,BindingResult bindingResult, SessionStatus status) throws InValidEncryptedIdException {
        logger.info("in updatePlayer of ProfileController");
        //Validating on server side
        playerValidator.validate(player,bindingResult);
        if(bindingResult.hasErrors()){
            return "site.profile";
        }
        //Going to update the profile
        player = playerService.updateProfile(playerIdStr, player);
        //Going to flush the data in session
        status.setComplete();
        model.addAttribute("player",player);
        model.addAttribute("message","Profile updated");
        return "site.profile";
    }
}
