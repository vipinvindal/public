package com.quarks.scrabble.dao;

import com.quarks.scrabble.model.BaseDataObject;
import org.springframework.data.domain.Pageable;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Base DAO
 * @author Ravinder Rathi
 */
public interface BaseDao<T extends BaseDataObject, ID extends Serializable> {

	/**
	 * Find by Id
	 */
    T findById(ID id, boolean lock);

    /**
	 * Find by Id
	 */
    T findById(ID id);

    /**
	 * Find All
	 */
    List<T> findAll();

    /**
	 * Persist data in batch
	 */
    void batchPersistent(Collection<? extends BaseDataObject> entities, Integer batchSize);

    /**
     * Persist entity
     * @param entity
     */
    T makePersistent(T entity);

    /**
     * Save or update the whole collection
     * @param entities
     */
    Collection<T> saveOrUpdateAll(Collection<T> entities);

    /**
     * Persist entity
     * @param entity
     */
    T persist(T entity);

    /**
     * Make transient
     * @param entity
     */
    void makeTransient(T entity);

    /**
     * Count the no of results as per the criteria
     * @param cq
     */
    Long countByCriteriaQuery(CriteriaQuery<Long> cq);

    /**
     * Get the criteria builder
     */
    CriteriaBuilder getCriteriaBuilder();

    /**
     * Delete entity
     * @param entity
     */
    void delete(T entity);

    /**
     * Flush session
     */
    void flush();

    /**
     * Clear session
     */
    void clear();

    /**
     * Get single result against the query, will return null if no result is found
     * @param typedQuery
     */
    T getSingleResult(TypedQuery<T> typedQuery);

    /**
     * Get a single record from the table
     * @return
     */
    T findOne();

    /**
     * Get single result from results
     * @param results
     * @return
     */
    T getSingleResult(List<T> results);

    /**
     * Load by ids
     * @param ids
     */
    List<T> findByIds(List<Long> ids);

    /**
     * Get paginated results as per the search criteria
     * @param cq
     * @param pageable
     * @return
     */
    List<T> findByCriteriaQuery(CriteriaQuery<T> cq,Pageable pageable);
}
