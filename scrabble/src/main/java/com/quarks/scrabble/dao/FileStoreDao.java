package com.quarks.scrabble.dao;


import com.quarks.scrabble.model.FileStore;

/**
 * DAO for FileStore
 * @author Quarks
 *
 */
public interface FileStoreDao extends BaseDao<FileStore,Long>{

}