package com.quarks.scrabble.dao;

import com.quarks.scrabble.model.Match;

import java.util.List;

/**
 * @author Ravinder Rathi
 */
public interface MatchDao extends BaseDao<Match,Long> {
	
	/**
	 * Load all the matches played by the player
	 * @param id
	 */
    List<Match> findAllMatchPlayedByPlayer(Long playerId);
}
