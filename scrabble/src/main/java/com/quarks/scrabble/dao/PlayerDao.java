package com.quarks.scrabble.dao;

import com.quarks.scrabble.model.Player;

/**
 * @author vipinvindal
 */
public interface PlayerDao extends BaseDao<Player,Long>{
	
	/**
	 * Find player by id
	 * @param id
	 */
    Player findPlayerById(Long id);
}
