package com.quarks.scrabble.dao;

import com.quarks.scrabble.model.PlayerStats;

import java.util.List;

/**
 * Created by apple on 2/25/17.
 */
public interface PlayerStatsDao extends BaseDao<PlayerStats,Long> {

	/**
	 * Get player stats
	 * @param playerId
	 */
    PlayerStats findPlayerStats(Long playerId);

    /**
     * Get leader board
     * @param leaderBoardSize
     * @param minMatchPlayed
     */
    List<PlayerStats> getLeaderBoard(Integer leaderBoardSize,Long minMatchPlayed);

}
