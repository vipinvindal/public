package com.quarks.scrabble.doa.impl;

import com.quarks.scrabble.dao.BaseDao;
import com.quarks.scrabble.model.BaseDataObject;
import com.quarks.utils.StringUtils;

import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Base Doa impl
 * This class have base methods
 * Created by apple on 2/25/17.
 */
public abstract class BaseDaoImpl<T extends BaseDataObject, ID extends Serializable> implements BaseDao<T, ID> {

    private Class<T> entityBeanType;

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public BaseDaoImpl() {
        this.entityBeanType = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Class<T> getEntityBeanType() {
        return entityBeanType;
    }

    public void setEntityBeanType(Class<T> entityBeanType) {
        this.entityBeanType = entityBeanType;
    }

    @Override
    public T findById(ID id, boolean lock) {
        T entity;
        if (lock) {
            entity = getEntityManager().find(getEntityBeanType(), id);
            entityManager.lock(entity, javax.persistence.LockModeType.WRITE);
        } else {
            entity = getEntityManager().find(getEntityBeanType(), id);
        }
        return entity;
    }

    @Override
    public T findById(ID id) {
        return findById(id, false);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> findAll() {
        return getEntityManager().createQuery("from " + getEntityBeanType().getName()).getResultList();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void batchPersistent(Collection<? extends BaseDataObject> entities, Integer batchSize) {
        int counter = 0;
        for (BaseDataObject entity : entities) {
            if (entity == null) {
                continue;
            }
            if (entity.getId() != null) {
                getEntityManager().merge(entity);
                continue;
            }
            getEntityManager().persist(entity);
            if (++counter % batchSize == 0) {
                getEntityManager().flush();
                getEntityManager().clear();
            }

        }
        getEntityManager().flush();
        getEntityManager().clear();
    }

    @Override
    public T makePersistent(T entity) {
        String username = "System";
        BaseDataObject baseObject = (BaseDataObject) entity;
        // createdBy, updatedBy, createdDate,updatedDate
        if (StringUtils.isNull(baseObject.getCreatedBy())) {
            baseObject.setCreatedBy(username);
        }
        if (baseObject.getCreatedDate() == null) {
            baseObject.setCreatedDate(new Date(System.currentTimeMillis()));
        }
        baseObject.setUpdatedDate(new Date(System.currentTimeMillis()));
        baseObject.setUpdatedBy(username);

        return getEntityManager().merge(entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Collection<T> saveOrUpdateAll(Collection<T> entities) {
        Collection<T> persistedEntities = null;
        if (entities instanceof Set) {
            persistedEntities = new HashSet<T>();
        } else {
            persistedEntities = new ArrayList<T>();
        }
        for (BaseDataObject entity : entities) {
            persistedEntities.add(makePersistent((T) entity));
        }
        return persistedEntities;
    }

    @Override
    public T persist(T entity) {
        T e = getEntityManager().merge(entity);
        getEntityManager().persist(e);
        getEntityManager().flush();
        return e;
    }

    @Override
    public void makeTransient(T entity) {
        getEntityManager().remove(entity);
    }

    @Override
    public void delete(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    @Override
    public void flush() {
        getEntityManager().flush();
    }

    @Override
    public void clear() {
        getEntityManager().clear();
    }

    @Override
    public T getSingleResult(TypedQuery<T> typedQuery) {
        try {
            return typedQuery.getSingleResult();
        } catch (NoResultException resultException) {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T findOne() {
        Query createQuery = getEntityManager().createQuery("from " + getEntityBeanType().getClass());
        createQuery.setMaxResults(1);
        try {
            return (T) createQuery.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    @Override
    public T getSingleResult(List<T> results) {
        if (results == null || results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> findByIds(List<Long> ids) {
        if(ids.isEmpty()){
            return new ArrayList<>();
        }
        String sql = "from " + getEntityBeanType().getClass() + " where id in (:ids)";
        Query createQuery = getEntityManager().createQuery(sql);
        createQuery.setParameter("ids", ids);
        return  createQuery.getResultList();
    }

    @Override
    public List<T> findByCriteriaQuery(CriteriaQuery<T> cq,Pageable pageable) {
        return getEntityManager().createQuery(cq)
                .setMaxResults(pageable.getPageSize())
                .setFirstResult(pageable.getOffset())
                .getResultList();
    }
    @Override
    public Long countByCriteriaQuery(CriteriaQuery<Long> cq) {
        return getEntityManager().createQuery(cq).getSingleResult();
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder(){
        return entityManager.getCriteriaBuilder();
    }
}