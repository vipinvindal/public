package com.quarks.scrabble.doa.impl;


import com.quarks.scrabble.dao.FileStoreDao;
import com.quarks.scrabble.model.FileStore;

import org.springframework.stereotype.Repository;

/**
 * Impl of FileStoreDAO
 * @author Quarks
 */
@Repository
public class FileStoreDaoImpl extends BaseDaoImpl<FileStore,Long> implements FileStoreDao {

}
