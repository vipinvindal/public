package com.quarks.scrabble.doa.impl;

import com.quarks.scrabble.dao.MatchDao;
import com.quarks.scrabble.model.Match;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;

import java.util.List;

/**
 * @author vipinvindal
 */
@Repository
public class MatchDaoImpl extends BaseDaoImpl<Match,Long> implements MatchDao {

    private static final Logger logger = LoggerFactory.getLogger(MatchDaoImpl.class);

    @Override
    public List<Match> findAllMatchPlayedByPlayer(Long playerId) {
        logger.info("Get all played matches for : " + playerId);
        String sql = "select match from Match match where match.player1.id =:playerId or match.player2.id =:playerId";
        logger.debug("sql : ", sql);
        TypedQuery<Match> typedQuery = getEntityManager().createQuery(sql,Match.class);
        typedQuery.setParameter("playerId",playerId);
        return typedQuery.getResultList();
    }
}
