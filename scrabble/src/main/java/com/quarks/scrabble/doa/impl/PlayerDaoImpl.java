package com.quarks.scrabble.doa.impl;

import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.quarks.scrabble.dao.PlayerDao;
import com.quarks.scrabble.model.Player;

/**
 * Created by apple on 2/25/17.
 */
@Repository
public class PlayerDaoImpl extends BaseDaoImpl<Player,Long> implements PlayerDao {

    private static final Logger logger = LoggerFactory.getLogger(PlayerDaoImpl.class);

    @Override
    public Player findPlayerById(Long id) {
        logger.info("find player for : " + id);
        String sql = "select p from Player p where p.id =:id";
        logger.debug("sql : ", sql);
        TypedQuery<Player> typedQuery = getEntityManager().createQuery(sql,Player.class);
        typedQuery.setParameter("id",id);
        return typedQuery.getSingleResult();
    }
}
