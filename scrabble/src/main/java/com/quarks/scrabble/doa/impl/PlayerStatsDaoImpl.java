package com.quarks.scrabble.doa.impl;

import com.quarks.scrabble.dao.PlayerStatsDao;
import com.quarks.scrabble.model.PlayerStats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;

import java.util.List;

/**
 * Created by apple on 2/25/17.
 */
@Repository
public class PlayerStatsDaoImpl extends BaseDaoImpl<PlayerStats,Long> implements PlayerStatsDao {

    private static final Logger logger = LoggerFactory.getLogger(PlayerStatsDaoImpl.class);

    @Override
    public PlayerStats findPlayerStats(Long playerId) {
        logger.info("load stats for : " + playerId);
        String sql = "select stats from PlayerStats stats where stats.player.id =:playerId";
        logger.debug("sql : ", sql);
        TypedQuery<PlayerStats> typedQuery = getEntityManager().createQuery(sql,PlayerStats.class);
        typedQuery.setParameter("playerId",playerId);
        return typedQuery.getSingleResult();
    }

    @Override
    public List<PlayerStats> getLeaderBoard(Integer leaderBoardSize, Long minMatchPlayed) {
        logger.info("Get leader board");
        String sql = "select stats from PlayerStats stats join fetch stats.highestScoreMatch where stats.noOfMatches>=:minMatchPlayed order by stats.averageScore desc";
        logger.debug("sql : ", sql);
        TypedQuery<PlayerStats> typedQuery = getEntityManager().createQuery(sql, PlayerStats.class);
        typedQuery.setParameter("minMatchPlayed",minMatchPlayed);
        typedQuery.setMaxResults(leaderBoardSize);
        return typedQuery.getResultList();
    }


}
