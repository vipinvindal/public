package com.quarks.scrabble.model;

import com.quarks.utils.EncryptionUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Date;

/**
 * This is a base class for all the models
 * @author Quarks
 */
@MappedSuperclass 
public class BaseDataObject implements Serializable {

	private static final long serialVersionUID = 7328890581475061311L;
	
	/**
	 * Primary Id of the object
	 */
	@Id  
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	protected Long id;
	
	/**
	 * Who created the object
	 */
	private String createdBy = "System";
	
	/**
	 * Who updated the object
	 */
	private String updatedBy = "System";
	
	/**
	 * When object is created
	 */
	private Date createdDate;
	
	/**
	 * When object is updated
	 */
	private Date updatedDate;

    @Transient
    private String encryptedId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    @Basic
    @Column(name = "createdBy", nullable = false, insertable = true, updatable = true, length = 50)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

    @Basic
    @Column(name = "updatedBy", nullable = false, insertable = true, updatable = true, length = 50)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

    @Basic
    @Column(name = "createdDate", nullable = false, insertable = true, updatable = true)
	public Date getCreatedDate() {
		return createdDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


    @Basic
    @Column(name = "updatedDate", nullable = false, insertable = true, updatable = true)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

    @PrePersist
    protected void onCreate() {
        createdDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedDate = new Date();
    }
    
	/**
	 * Returns the encrypted Id of the job
	 * @return  encodedId
	 */
	@SuppressWarnings("deprecation")
	public String getEncodedId(){
		String base64EncodedId = EncryptionUtils.encrypt(getId().toString());
		String urlEncodedId = URLEncoder.encode(base64EncodedId);
		return urlEncodedId;
	}
	
	/**
	 * Returns only base64 encrypted id
	 * @return encryptedId
	 */
	public String getEncryptedId(){
        if(getId() != null){
            String base64EncodedId = EncryptionUtils.encrypt(getId().toString());
            return base64EncodedId;
        }
		return this.encryptedId;
	}

    public void setEncryptedId(String encryptedId) {
        this.encryptedId = encryptedId;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((updatedBy == null) ? 0 : updatedBy.hashCode());
		result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseDataObject other = (BaseDataObject) obj;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (updatedBy == null) {
			if (other.updatedBy != null)
				return false;
		} else if (!updatedBy.equals(other.updatedBy))
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		return true;
	}
}
