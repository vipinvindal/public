package com.quarks.scrabble.model;

import java.io.File;
import java.io.IOException;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

import com.quarks.constant.QuarksConstants;
import com.quarks.utils.EncryptionUtils;
import com.quarks.utils.PropertyFileReader;


/**
 * Model represents a file entry in the store
 * Possible File Types - Tender PDF, Pan, Address Proof and Digital Signature
 * @author Quarks
 */
@Entity
@Table(name = "filestore")
public class FileStore extends BaseDataObject{
	
	private static final long serialVersionUID = 3757601473482022972L;
	
	/**
	 * Name of the file
	 */
	private String filename;
	/**
	 * Path of the file
	 */
	private String path;
	
	@Transient
	private byte[] content;
	
	@Transient
	private MultipartFile file;
	
	/**
	 * Returns Encrypted Id of file store
	 */
	public String getEncryptedId() {
		return EncryptionUtils.encrypt(getId().toString());
	}
	
	public String getEncryptedImageUrl() {
		return QuarksConstants.FILESERVER_PATH + getEncryptedId();
	}
	
	/**
	 * Rich Method to create url
	 * @return apacheUrl
	 */
	public String getApacheUrl(){
		String contextPath = PropertyFileReader.getValue(QuarksConstants.QUARKS_HOST);
		return contextPath + QuarksConstants.FILESERVER_PATH + getEncryptedId();
	}
	
	/**
	 * Rich Method to get path
	 */
	public String getFullPath(){
		String fp = getPath();
		String fn = getFilename();
		return "/"+fp + "/" + fn;
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

    public File getActualFile() throws IOException {
        String NDATAPATH = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);
        if(getPath()!=null){
            return new File(NDATAPATH + File.separator + getPath() + File.separator + getFilename());
        }
        return null;
    }

	@Override
	public String toString() {
		return "FileStore [filename=" + filename + ", path=" + path + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((filename == null) ? 0 : filename.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileStore other = (FileStore) obj;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}
}
