package com.quarks.scrabble.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Entity representing a match between two players
 * @author Vipin Kumar Vindal
 *
 */
@Entity
@javax.persistence.Table(name = "match", schema = "", catalog = "scrabble")
public class Match extends BaseDataObject{
	
	private static final long serialVersionUID = 8127881231358832165L;
	
	/**
	 * Player 1 of the match
	 */
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player1",referencedColumnName = "id")
	private Player player1;
	private Long player1Score;
	
	/**
	 * Player 2 of the match
	 */
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player2",referencedColumnName = "id")
	private Player player2;
	private Long player2Score;
	
	public Player getPlayer1() {
		return player1;
	}
	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}
	public Long getPlayer1Score() {
		return player1Score;
	}
	public void setPlayer1Score(Long player1Score) {
		this.player1Score = player1Score;
	}
	public Player getPlayer2() {
		return player2;
	}
	public void setPlayer2(Player player2) {
		this.player2 = player2;
	}
	public Long getPlayer2Score() {
		return player2Score;
	}
	public void setPlayer2Score(Long player2Score) {
		this.player2Score = player2Score;
	}
	@Override
	public String toString() {
		return "Match [player1=" + player1 + ", player1Score=" + player1Score
				+ ", player2=" + player2 + ", player2Score=" + player2Score
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((player1 == null) ? 0 : player1.hashCode());
		result = prime * result
				+ ((player1Score == null) ? 0 : player1Score.hashCode());
		result = prime * result + ((player2 == null) ? 0 : player2.hashCode());
		result = prime * result
				+ ((player2Score == null) ? 0 : player2Score.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Match other = (Match) obj;
		if (player1 == null) {
			if (other.player1 != null)
				return false;
		} else if (!player1.equals(other.player1))
			return false;
		if (player1Score == null) {
			if (other.player1Score != null)
				return false;
		} else if (!player1Score.equals(other.player1Score))
			return false;
		if (player2 == null) {
			if (other.player2 != null)
				return false;
		} else if (!player2.equals(other.player2))
			return false;
		if (player2Score == null) {
			if (other.player2Score != null)
				return false;
		} else if (!player2Score.equals(other.player2Score))
			return false;
		return true;
	}
	

}
