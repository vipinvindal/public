package com.quarks.scrabble.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Entity represents a player
 * @author Vipin Kumar Vindal
 *
 */
@Entity
@javax.persistence.Table(name = "player", schema = "", catalog = "scrabble")
public class Player extends BaseDataObject{

	private static final long serialVersionUID = 6619210047899499479L;
	
	/**
	 * Full name of the player
	 */
	private String name;
	
	/**
	 * Email of the player
	 */
	private String email;
	
	/**
	 * Is Email Verified
	 */
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isEmailVerified = Boolean.FALSE;
	
	/**
	 * Mobile of the player
	 */
	private String mobile;
	
	/**
	 * Is Mobile Verified
	 */
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean isMobileVerified = Boolean.FALSE;
	
	/**
	 * MD5 hash of the password
	 */
	private String passwordHash;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "profilePicture",referencedColumnName = "id")
	private FileStore profilePicture;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Boolean getIsMobileVerified() {
		return isMobileVerified;
	}

	public void setIsMobileVerified(Boolean isMobileVerified) {
		this.isMobileVerified = isMobileVerified;
	}

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public FileStore getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(FileStore profilePicture) {
		this.profilePicture = profilePicture;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", email=" + email
				+ ", isEmailVerified=" + isEmailVerified + ", mobile=" + mobile
				+ ", isMobileVerified=" + isMobileVerified + ", passwordHash="
				+ passwordHash + ", profilePicture=" + profilePicture + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((isEmailVerified == null) ? 0 : isEmailVerified.hashCode());
		result = prime
				* result
				+ ((isMobileVerified == null) ? 0 : isMobileVerified.hashCode());
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((passwordHash == null) ? 0 : passwordHash.hashCode());
		result = prime * result
				+ ((profilePicture == null) ? 0 : profilePicture.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (isEmailVerified == null) {
			if (other.isEmailVerified != null)
				return false;
		} else if (!isEmailVerified.equals(other.isEmailVerified))
			return false;
		if (isMobileVerified == null) {
			if (other.isMobileVerified != null)
				return false;
		} else if (!isMobileVerified.equals(other.isMobileVerified))
			return false;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (passwordHash == null) {
			if (other.passwordHash != null)
				return false;
		} else if (!passwordHash.equals(other.passwordHash))
			return false;
		if (profilePicture == null) {
			if (other.profilePicture != null)
				return false;
		} else if (!profilePicture.equals(other.profilePicture))
			return false;
		return true;
	}

}
