package com.quarks.scrabble.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Stats of a player
 * @author Vipin Kumar Vindal
 *
 */
@Entity
@javax.persistence.Table(name = "player_stats", schema = "", catalog = "scrabble")
public class PlayerStats extends BaseDataObject{
	
	private static final long serialVersionUID = 2964595109916379201L;
	
	/**
	 * Player whose stats are being pulled
	 */
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player",referencedColumnName = "id")
	private Player player;
	
	/**
	 * How many matches player has played
	 */
	private Long noOfMatches;
	/**
	 * How many wins player has made
	 */
	private Long noOfWins;
	/**
	 * How many loses player has made
	 */
	private Long noOfLoses;
	/**
	 * Average score of the player
	 */
	private Double averageScore;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "highestScoreMatch",referencedColumnName = "id")
	private Match highestScoreMatch;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Long getNoOfMatches() {
		return noOfMatches;
	}

	public void setNoOfMatches(Long noOfMatches) {
		this.noOfMatches = noOfMatches;
	}

	public Long getNoOfWins() {
		return noOfWins;
	}

	public void setNoOfWins(Long noOfWins) {
		this.noOfWins = noOfWins;
	}

	public Long getNoOfLoses() {
		return noOfLoses;
	}

	public void setNoOfLoses(Long noOfLoses) {
		this.noOfLoses = noOfLoses;
	}

	public Double getAverageScore() {
		return averageScore;
	}

	public void setAverageScore(Double averageScore) {
		this.averageScore = averageScore;
	}

	public Match getHighestScoreMatch() {
		return highestScoreMatch;
	}

	public void setHighestScoreMatch(Match highestScoreMatch) {
		this.highestScoreMatch = highestScoreMatch;
	}

	@Override
	public String toString() {
		return "PlayerStats [player=" + player + ", noOfMatches=" + noOfMatches
				+ ", noOfWins=" + noOfWins + ", noOfLoses=" + noOfLoses
				+ ", averageScore=" + averageScore + ", highestScoreMatch="
				+ highestScoreMatch + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((averageScore == null) ? 0 : averageScore.hashCode());
		result = prime
				* result
				+ ((highestScoreMatch == null) ? 0 : highestScoreMatch
						.hashCode());
		result = prime * result
				+ ((noOfLoses == null) ? 0 : noOfLoses.hashCode());
		result = prime * result
				+ ((noOfMatches == null) ? 0 : noOfMatches.hashCode());
		result = prime * result
				+ ((noOfWins == null) ? 0 : noOfWins.hashCode());
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlayerStats other = (PlayerStats) obj;
		if (averageScore == null) {
			if (other.averageScore != null)
				return false;
		} else if (!averageScore.equals(other.averageScore))
			return false;
		if (highestScoreMatch == null) {
			if (other.highestScoreMatch != null)
				return false;
		} else if (!highestScoreMatch.equals(other.highestScoreMatch))
			return false;
		if (noOfLoses == null) {
			if (other.noOfLoses != null)
				return false;
		} else if (!noOfLoses.equals(other.noOfLoses))
			return false;
		if (noOfMatches == null) {
			if (other.noOfMatches != null)
				return false;
		} else if (!noOfMatches.equals(other.noOfMatches))
			return false;
		if (noOfWins == null) {
			if (other.noOfWins != null)
				return false;
		} else if (!noOfWins.equals(other.noOfWins))
			return false;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		return true;
	}

}
