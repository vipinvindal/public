package com.quarks.scrabble.service;



import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.model.BaseDataObject;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Created by apple on 12/20/16.
 */
public interface BaseService <T1 extends BaseDataObject, T2 extends Serializable>{

    /**
     * Save  or update entity
     * @param entity
     * @return
     */
    T1 saveOrUpdate(T1 entity) ;

    /**
     * find by identity
     * @param id
     * @return
     */
    T1 findById(T2 id);

    /**
     * delete entity
     * @param entity
     */
    void delete(T1 entity);

    /**
     * save in batch
     * @param entities entity to save
     * @param batchSize size of batch
     */
    void batchPersistent(Collection<T1> entities, Integer batchSize) ;

    /**
     * save or update list of entities
     * @param entities
     * @return
     */
    List<T1> saveOrUpdateAll(List<T1> entities);

    /**
     * find all entity in table
     * @return
     */
    List<T1> findAll();

    /**
     * get one entity order by identity
     * @return
     */
    T1 findOne();

    /**
     * delete entities
     * @param entities
     */
    void deleteAll(List<T1> entities);

    /**
     * flush entity manager
     */
    void flush();

    /**
     * generate encryted identity
     * @param idStr
     * @return
     * @throws InValidEncryptedIdException
     */
    T2 getDecryptedId(String idStr) throws InValidEncryptedIdException;
}