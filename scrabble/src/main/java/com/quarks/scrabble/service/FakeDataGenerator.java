package com.quarks.scrabble.service;

import com.github.javafaker.Faker;
import com.quarks.scrabble.model.Match;
import com.quarks.scrabble.model.Player;
import com.quarks.scrabble.model.PlayerStats;
import com.quarks.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * This class for generate fake data
 * Created by apple on 2/26/17.
 */
@Component
public class FakeDataGenerator {

    /**
     * number of player to generate
     */
    private static final Integer MAX_PLAYER_TO_GENERATE = 100;

    /**
     * Max match played by any player, this is for creating between players
     */
    private static final Integer MAX_MATCH_PLAYED_BY_ANY_PLAYER = 25;

    /**
     * first match date, for match data generations
     */
    private static final Date FIRST_MATCH_PLAYED_DATE = DateUtils.asDate(LocalDate.of(2010, 01, 14));

    /**
     * last match played
     */
    private static final Date LAST_MATCH_PLAYED = new Date();

    /**
     * min score for any player
     */
    private static final Long MIN_SCORE_OF_ANY_PLAYER = 0L;

    /**
     * max score by any player
     */
    private static final Long MAX_SCORE_OF_ANY_PLAYER = 1000L;

    /**
     * temp password key
     */
    private static final String PASSWORD_STRING = "secret";


    @Autowired
    MatchService matchService;

    @Autowired
    PlayerService playerService;

    @Autowired
    PlayerStatsService playerStatsService;

    /**
     * this method generate player,match,calculate stats and save to db
     *
     */
    @Transactional
    public void generateFakeData(){
        List<Player> players = generatePlayer();
        playerService.batchPersistent(players,10);
        List<Match> matches = generateMatch();
        matchService.batchPersistent(matches,50);
        List<PlayerStats> stats = generateStatsFromDb();
        playerStatsService.batchPersistent(stats, 50);
    }

    /**
     * this method build fake players
     * @return list of players
     */
    public List<Player> generatePlayer(){
        Faker faker = new Faker();
        List<Player> players = new LinkedList<>();
        Player player;

        String passwordHash = getPasswordHash();
        for (long i = 1; i <= MAX_PLAYER_TO_GENERATE; i++) {
            player = new Player();
            player.setEmail(faker.internet().emailAddress());
            player.setMobile(faker.phoneNumber().cellPhone());
            player.setName(faker.name().name());
            player.setPasswordHash(passwordHash);
            players.add(player);
        }
        return players;
    }

    /**
     * this method build match between player
     * @return list of match
     */
    public List<Match> generateMatch(){
        Faker faker = new Faker();
        Player player2;
        Long player1Score;
        Long player2Score;
        Match match;
        List<Match> matches = new LinkedList<>();
        List<Player> players = playerService.findAll();
        Random rnd = new Random();
        for(Player player : players){
            //set how many match this player should play
            int numberOfMatchPlayed = rnd.nextInt(MAX_MATCH_PLAYED_BY_ANY_PLAYER);
            while(numberOfMatchPlayed > 0) {
                match = new Match();
                player2 = getRandomObject(players);
                while (player2.equals(player)) {
                    player2 = getRandomObject(players);
                }
                match.setPlayer1(player);
                match.setPlayer2(player2);
                player1Score = Long.valueOf(faker.number().numberBetween(MIN_SCORE_OF_ANY_PLAYER, MAX_SCORE_OF_ANY_PLAYER));
                player2Score = Long.valueOf(faker.number().numberBetween(MIN_SCORE_OF_ANY_PLAYER, MAX_SCORE_OF_ANY_PLAYER));
                Date datePlayed = faker.date().between(FIRST_MATCH_PLAYED_DATE, LAST_MATCH_PLAYED);
                match.setPlayer1Score(player1Score);
                match.setPlayer2Score(player2Score);
                match.setCreatedDate(datePlayed);
                matches.add(match);
                numberOfMatchPlayed--;
            }
        }
        return matches;
    }

    /**
     * this method calculate player stats
     * @return
     */
    public List<PlayerStats> generateStatsFromDb(){
        List<Player> players = playerService.findAll();
        List<PlayerStats> stats = new LinkedList<>();
        for(Player player : players){
            List<Match> allMatchPlayedByPlayer = matchService.findAllMatchPlayedByPlayer(player.getId());
            Double totalScore = 0.0;
            Long maxScore = 0L;
            Long minScore = 0L;
            Long totalWins = 0L;
            Long totalLoss = 0L;
            Match maxScoredMatch = null;
            Double avgScore = 0.0;
            PlayerStats playerStats;
            for(Match match : allMatchPlayedByPlayer){
                if(match.getPlayer1().equals(player)){
                    if(maxScore <= match.getPlayer1Score()){
                        maxScore = match.getPlayer1Score();
                        maxScoredMatch = match;
                    }
                    if(minScore >= match.getPlayer1Score()){
                        minScore = match.getPlayer1Score();
                    }
                    totalScore = totalScore + match.getPlayer1Score();
                    if(match.getPlayer2Score() > match.getPlayer1Score()){
                        totalLoss = totalLoss + 1;
                    }else{
                        totalWins = totalWins + 1;
                    }
                }else {
                    if(maxScore <= match.getPlayer2Score()){
                        maxScore = match.getPlayer2Score();
                        maxScoredMatch = match;
                    }
                    if(minScore >= match.getPlayer2Score()){
                        minScore = match.getPlayer2Score();
                    }
                    totalScore = totalScore + match.getPlayer2Score();
                    if(match.getPlayer1Score() > match.getPlayer2Score()){
                        totalLoss = totalLoss + 1;
                    }else{
                        totalWins = totalWins + 1;
                    }
                }
            }
            avgScore = (totalScore/allMatchPlayedByPlayer.size());
            playerStats = new PlayerStats();
            playerStats.setPlayer(player);
            playerStats.setAverageScore(avgScore);
            playerStats.setHighestScoreMatch(maxScoredMatch);
            playerStats.setNoOfLoses(totalLoss);
            playerStats.setNoOfWins(totalWins);
            playerStats.setNoOfMatches(Long.valueOf(allMatchPlayedByPlayer.size()));
            stats.add(playerStats);

        }
        return stats;
    }

    /**
     * generate password hash
     * @return md5 hash of string
     */
    private String getPasswordHash(){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(PASSWORD_STRING.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashText = number.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashText.length() < 32) {
                hashText = "0" + hashText;
            }
            return hashText;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * this method select rendom element from list
     * @param from is list
     * @param <T> type of object in list
     * @return random element
     */
    private <T> T getRandomObject(List<T> from) {
        Random rnd = new Random();
        int i = rnd.nextInt(from.size());
        return from.get(i);
    }
}
