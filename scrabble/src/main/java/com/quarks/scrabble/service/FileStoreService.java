package com.quarks.scrabble.service;

import com.quarks.scrabble.model.FileStore;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Service class for FileStore
 * @author Quarks
 */
public interface FileStoreService extends BaseService<FileStore,Long>{
	
	/**
	 * Saves a file in filestore
	 * @param fs
	 * @return filestore object
	 * @throws Exception
	 */
	public FileStore saveFile(FileStore fs) throws Exception;
	
	/**
	 * Find By Ids
	 * @Param filestoreIds
	 */
	public List<FileStore> findByIds(List<Long> ids);
	
	
	/**
	 * Retreived a filestore object
	 * @param id
	 * @return filestore
	 * @throws Exception
	 */
	public FileStore getFile(Long id) throws Exception;
	
	/**
	 * Retreived a filestore object
	 * @param id
	 * @return filestore
	 * @throws Exception
	 */
	public FileStore getFileWithoutContent(Long id) throws Exception;
	
	/**
	 * Get Filehandle for this filestore
	 * @param fs
	 * @return
	 * @throws java.io.IOException
	 */
	public File getFileHandle(FileStore fs) throws IOException ;
	
	/**
	 * Deletes a filestore object
	 * @param id
	 * @throws Exception
	 */
	public void deleteFile(Long id) throws Exception;
	
	/**
	 * Deletes file from Disk if exists
	 */
	public void deleteFile(String filePath) throws Exception;
	
	/**
	 * Deletes a filestore object at database level
	 * @param id
	 * @throws Exception
	 */
	public void deleteFileStoreOnlyFromDb(Long id) throws Exception;
	
	/**
	 * Saves a file to disk
	 * @param fs
	 * @throws Exception
	 */
	public void saveFileToDiskOnly(FileStore fs) throws Exception;

	/**
	 * Save File to a file path
	 * @param multipartFile
	 * @param filePath
	 * @throws Exception
	 */
	public FileStore saveFile(MultipartFile multipartFile, String filePath) throws Exception;

	/**
	 * Save File to a file path
	 * @param file
	 * @param filePath
	 * @throws Exception
	 */
    public FileStore saveFile(File file, String filePath)	throws Exception;

}
