package com.quarks.scrabble.service;

import com.quarks.scrabble.model.Match;

import java.util.List;

/**
 * Match service interface
 * @author vipinvindal
 */
public interface MatchService extends BaseService<Match,Long> {
	
    /**
     * Get match by id(primary key)
     * @param id (primary key)
     * @return List of all match played by player
     */
    List<Match> findAllMatchPlayedByPlayer(Long id);
}
