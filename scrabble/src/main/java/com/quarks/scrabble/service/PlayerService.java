package com.quarks.scrabble.service;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.model.Player;

/**
 * Player Server interface
 * @author vipinvindal
 */
public interface PlayerService extends BaseService<Player,Long> {

    /**
     * Get player by encrypted id
     * @param id
     * @return Player
     * @throws InValidEncryptedIdException
     */
    Player getPlayer(String id) throws InValidEncryptedIdException;

    /**
     * Update player
     * @param playerIdStr encrypted id of player
     * @param player
     * @return Player updated player
     * @throws InValidEncryptedIdException
     */
    Player updateProfile(String playerIdStr, Player player) throws InValidEncryptedIdException;
}
