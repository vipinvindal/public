package com.quarks.scrabble.service;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.model.PlayerStats;

import java.util.List;

/**
 * PlayerStats service interface
 * Created by apple on 2/25/17.
 */
public interface PlayerStatsService extends BaseService<PlayerStats,Long> {

    /**
     * get player stats by player encrypted id
     * @param playerId
     * @return playerStats
     */
    PlayerStats getPlayerStats(Long playerId);

    /**
     * Get LeaderBoard by average score
     * player only qualify if played more then 10 matches
     * @return return list of size 10
     */
    List<PlayerStats> getLeaderBoard();

    /**
     * get Player stats
     * @param playerIdStr player encrypted id
     * @return
     * @throws InValidEncryptedIdException when id is invalid
     */

    PlayerStats getPlayerStats(String playerIdStr) throws InValidEncryptedIdException;
}
