package com.quarks.scrabble.service.impl;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.quarks.scrabble.dao.BaseDao;
import com.quarks.scrabble.model.BaseDataObject;
import com.quarks.scrabble.service.BaseService;

/**
 * Impl of base service
 * @author vipinvindal
 */
public abstract class BaseServiceImpl<T extends BaseDao<T1, T2>, T1 extends BaseDataObject, T2 extends Serializable> implements BaseService<T1, T2> {

    protected T baseDao;

    BaseServiceImpl(T baseDao) {
        this.baseDao = baseDao;
    }

    @Override
    @Transactional
    public void batchPersistent(Collection<T1> entities, Integer batchSize) {
        baseDao.batchPersistent(entities, batchSize);
    }

    @Override
    @Transactional
    public T1 saveOrUpdate(T1 entity) {
        return baseDao.makePersistent(entity);
    }

    @Override
    @Transactional
    public List<T1> saveOrUpdateAll(List<T1> entities) {
        List<T1> newEntities = new ArrayList<T1>();
        for(T1 entity: entities){
            newEntities.add(baseDao.makePersistent(entity));
        }
        return newEntities;
    }

    @Override
    public T1 findById(T2 id) {
        return baseDao.findById(id);
    }

    @Override
    public List<T1> findAll() {
        return baseDao.findAll();
    }

    @Override
    @Transactional
    public void delete(T1 entity) {
        baseDao.delete(entity);
    }

    @Override
    public T1 findOne(){
        return baseDao.findOne();
    }

    @Override
    public void deleteAll(List<T1> entities) {
        for(T1 entity: entities){
            baseDao.delete(entity);
        }

    }
    @Override
    public void flush(){
        baseDao.flush();
    }

}