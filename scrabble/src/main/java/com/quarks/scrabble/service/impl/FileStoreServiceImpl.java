package com.quarks.scrabble.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.quarks.constant.QuarksConstants;
import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.dao.FileStoreDao;
import com.quarks.scrabble.model.FileStore;
import com.quarks.scrabble.service.FileStoreService;
import com.quarks.utils.DateUtils;
import com.quarks.utils.EncryptionUtils;
import com.quarks.utils.StringUtils;


/**
 * Impl of FileStoreService
 * @author Quarks
 */
@Service
@Transactional ( readOnly = true )
public class FileStoreServiceImpl extends BaseServiceImpl<FileStoreDao,FileStore,Long> implements FileStoreService {

	@Autowired
    FileStoreServiceImpl(FileStoreDao baseDao) {
        super(baseDao);
    }

    @Override
	public FileStore saveFile(FileStore fs) throws Exception{
		String NDATAPATH = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);
		fs = doPreProcessing(fs);
		if(fs.getPath()!=null && fs.getFilename()!=null && fs.getPath()!="" && fs.getFilename()!="")
		{
			 File theDir = new File(NDATAPATH + fs.getPath());
			 if(theDir.exists() && !theDir.isDirectory()){
				 throw new Exception("Inconsistent Filesystem");
			 }
			 if (!theDir.exists()){
				 theDir.mkdirs();
			 }
			 if(fs.getContent() != null) {
				 File theFile = new File(NDATAPATH + "/" + fs.getPath()+"/"+fs.getFilename());
				 FileUtils.writeByteArrayToFile(theFile, fs.getContent()); 
			 }
			 fs.setContent(null);
		}
		return baseDao.makePersistent(fs);
	}


	@Override
	public FileStore getFile(Long id) throws IOException {
		String NDATAPATH = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);
		FileStore fs = baseDao.findById(id);
		if(fs.getPath()!=null){
			fs.setContent(FileUtils.readFileToByteArray(new File(NDATAPATH + File.separator + fs.getPath()+ File.separator +fs.getFilename())));
		}
		return fs;
	}
	
	@Override
	public File getFileHandle(FileStore fs) throws IOException {
		String NDATAPATH = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);
		return 	new File(NDATAPATH + File.separator + fs.getPath()+ File.separator + fs.getFilename());
	}
	
	@Override
	public FileStore getFileWithoutContent(Long id) throws Exception{
		return baseDao.findById(id);
	}
	
	@Override
	public void deleteFile(Long id) throws Exception {
		String NDATAPATH = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);
		FileStore fs = getFileWithoutContent(id);
		if(fs.getPath()!=null){
			File file = new File(NDATAPATH + "/" +fs.getPath()+"/"+fs.getFilename());
			file.delete();
		}
		/*baseDao.delete(fs);*/
	}
	
	@Override
	public void deleteFile(String filePath) throws Exception{
		File file = new File(filePath);
		 if(file.exists()) {
			 file.delete();
		 }
	}
	
	/**
	 * Deletes a filestore object
	 * @param id
	 * @throws Exception
	 */
	public void deleteFileStoreOnlyFromDb(Long id) throws Exception{
		//FileStore fs = baseDao.findById(id);
		//baseDao.delete(fs);
	}

	@Override
	public void saveFileToDiskOnly(FileStore fs) throws Exception {
		String NDATAPATH = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);//PropertyFileReader.getValue("nDataPath");
		if(fs.getPath()!=null && fs.getFilename()!=null && fs.getPath()!="" && fs.getFilename()!=""){
			 File theDir = new File(NDATAPATH + fs.getPath());
			 if(theDir.exists() && !theDir.isDirectory()) {
				 throw new Exception("Inconsistent Filesystem");
			 }
			 if (!theDir.exists()) {
				 theDir.mkdirs();
			 }
			 File theFile = new File(NDATAPATH + "/" + fs.getPath()+"/"+fs.getFilename());
			 FileUtils.writeByteArrayToFile(theFile, fs.getContent());
			 fs.setContent(FileUtils.readFileToByteArray(theFile));
		}
	}
	
	@Override
	public FileStore saveFile(MultipartFile multipartFile, String filePath)	throws Exception {
		FileStore fs = null;
		filePath = doPreProcessing(filePath);
		String systemPath = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);
		String fileName = multipartFile.getOriginalFilename();
		if (StringUtils.isNotNull(fileName)) {
			fs = new FileStore();
			File theFile = new File(systemPath + "/" + filePath + "/" + fileName);
			if (filePath.equals(QuarksConstants.TEMP_LOCATION)) {
				// In case of temp path each server /tmp directory will be used
				theFile = new File(filePath + "/" + fileName);
			}
			if (!theFile.exists()) {
				fs.setFilename(fileName);
			} else {
				if (fileName.contains(".")) {
					fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_" + System.currentTimeMillis()
							+ fileName.substring(fileName.lastIndexOf("."), fileName.length());
				} else {
					fileName = fileName + "_" + System.currentTimeMillis();
				}
				fs.setFilename(fileName);
			}
			fs.setPath(filePath);
			fs.setContent(multipartFile.getBytes());
			fs = saveFile(fs);
		}
		return fs;
	}

    @Override
    public FileStore saveFile(File file, String filePath)	throws Exception {
        FileStore fs = null;
        filePath = doPreProcessing(filePath);
        String systemPath = System.getenv(QuarksConstants.NDATA_SYSTEM_PATH_PROPERTY);
        String fileName = file.getName();
        if (StringUtils.isNotNull(fileName)) {
            fs = new FileStore();
            File theFile = new File(systemPath + "/" + filePath + "/" + fileName);
            if (filePath.equals(QuarksConstants.TEMP_LOCATION)) {
                // In case of temp path each server /tmp directory will be used
                theFile = new File(filePath + "/" + fileName);
            }
            if (!theFile.exists()) {
                fs.setFilename(fileName);
            } else {
                if (fileName.contains(".")) {
                    fileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_" + System.currentTimeMillis()
                            + fileName.substring(fileName.lastIndexOf("."), fileName.length());
                } else {
                    fileName = fileName + "_" + System.currentTimeMillis();
                }
                fs.setFilename(fileName);
            }
            fs.setPath(filePath);
            Path path = Paths.get(file.getPath());
            fs.setContent(Files.readAllBytes(path));
            fs = saveFile(fs);
        }
        return fs;
    }
	
	/**
	 * Changes file path automatically with date structure
	 * @param fs
	 */
	private FileStore doPreProcessing(FileStore fs) {
		String filePath = fs.getPath();
		filePath = doPreProcessing(filePath);
		fs.setPath(filePath);
		return fs;
	}
	
	/**
	 * Changes file path automatically with date structure
	 */
	private String doPreProcessing(String filePath) {
		filePath = filePath.replaceAll("\\{curYear\\}", DateUtils.getCurYear());
		filePath = filePath.replaceAll("\\{curMonth\\}", DateUtils.getCurMonth());
		filePath = filePath.replaceAll("\\{curDate\\}", DateUtils.getCurDate());
		return filePath;
	}

	@Override
	public List<FileStore> findByIds(List<Long> ids) {
		return baseDao.findByIds(ids);
	}

    @Override
    public Long getDecryptedId(String idStr) throws InValidEncryptedIdException {
        try{
            String decryptedId = EncryptionUtils.decrypt(idStr);
            Long id = Long.valueOf(decryptedId);
            return id;
        }catch (NumberFormatException | NullPointerException exception){
            throw new InValidEncryptedIdException();
        }
    }
}
