package com.quarks.scrabble.service.impl;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.dao.MatchDao;
import com.quarks.scrabble.model.Match;
import com.quarks.scrabble.service.MatchService;
import com.quarks.utils.EncryptionUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Impl of match service
 * @author vipinvindal
 */
@Service
@Transactional( readOnly = true )
public class MatchServiceImpl extends BaseServiceImpl<MatchDao, Match,Long> implements MatchService {
    private static final Logger logger = LoggerFactory.getLogger(MatchServiceImpl.class);

    MatchServiceImpl(MatchDao baseDao) {
        super(baseDao);
    }

    @Override
    public Long getDecryptedId(String idStr) throws InValidEncryptedIdException {
        try{
            String decryptedId = EncryptionUtils.decrypt(idStr);
            Long matchId = Long.valueOf(decryptedId);
            return matchId;
        }catch (NumberFormatException | NullPointerException exception){
            logger.error("Exception while decrypting id : " + idStr);
            throw new InValidEncryptedIdException();
        }
    }

    @Override
    public List<Match> findAllMatchPlayedByPlayer(Long id) {
        logger.info("getPlayer in MatchServiceImpl fro id : " + id);
        return baseDao.findAllMatchPlayedByPlayer(id);
    }
}
