package com.quarks.scrabble.service.impl;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.dao.PlayerDao;
import com.quarks.scrabble.model.Player;
import com.quarks.scrabble.service.PlayerService;
import com.quarks.utils.EncryptionUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Impl of player service
 * @author vipinvindal
 */
@Service
@Transactional( readOnly = true )
public class PlayerServiceImpl extends BaseServiceImpl<PlayerDao, Player,Long> implements PlayerService {

    private static final Logger logger = LoggerFactory.getLogger(PlayerServiceImpl.class);

    PlayerServiceImpl(PlayerDao baseDao) {
        super(baseDao);
    }

    @Override
    public Player getPlayer(String id) throws InValidEncryptedIdException {
        logger.info("getPlayer in PlayerServiceImpl fro id : " + id);
        return baseDao.findPlayerById(getDecryptedId(id));
    }


    @Override
    @Transactional
    public Player updateProfile(String playerIdStr, Player player) throws InValidEncryptedIdException {
        logger.info("updateProfile in PlayerServiceImpl  : ",player);
        Long playerId = getDecryptedId(playerIdStr);
        player.setId(playerId);
        return saveOrUpdate(player);
    }

    @Override
    public Long getDecryptedId(String idStr) throws InValidEncryptedIdException {
        try{
            String decryptedId = EncryptionUtils.decrypt(idStr);
            Long playedId = Long.valueOf(decryptedId);
            return playedId;
        }catch (NumberFormatException | NullPointerException exception){
            logger.error("Exception while decrypting id : " + idStr);
            throw new InValidEncryptedIdException();
        }

    }
}
