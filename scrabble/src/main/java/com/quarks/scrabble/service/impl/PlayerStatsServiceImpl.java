package com.quarks.scrabble.service.impl;

import com.quarks.constant.QuarksConstants;
import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.dao.PlayerStatsDao;
import com.quarks.scrabble.model.PlayerStats;
import com.quarks.scrabble.service.PlayerStatsService;
import com.quarks.utils.EncryptionUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Impl of player stats service
 * @author vipinvindal
 */
@Service
@Transactional( readOnly = true )
public class PlayerStatsServiceImpl extends BaseServiceImpl<PlayerStatsDao, PlayerStats,Long> implements PlayerStatsService {

    private static final Logger logger = LoggerFactory.getLogger(PlayerStatsServiceImpl.class);

    PlayerStatsServiceImpl(PlayerStatsDao baseDao) {
        super(baseDao);
    }

    @Override
    public PlayerStats getPlayerStats(Long playerId) {
        logger.info("getting player stats for : " + playerId);
        return baseDao.findPlayerStats(playerId);
    }

    @Override
    public List<PlayerStats> getLeaderBoard() {
        logger.info("getting leader board");
        return baseDao.getLeaderBoard(QuarksConstants.LEADER_BOARD_LIMIT,QuarksConstants.LEADER_BOARD_MIN_MATCH_PLAYED);
    }

    @Override
    public PlayerStats getPlayerStats(String playerIdStr) throws InValidEncryptedIdException {
        logger.info("getting player stats for : " + playerIdStr);
        Long id = getDecryptedId(playerIdStr);
        return baseDao.findPlayerStats(id);
    }

    @Override
    public Long getDecryptedId(String idStr) throws InValidEncryptedIdException {
        try{
            String decryptedId = EncryptionUtils.decrypt(idStr);
            Long id = Long.valueOf(decryptedId);
            return id;
        }catch (NumberFormatException | NullPointerException exception){
            logger.error("Exception while decrypting id : " + idStr);
            throw new InValidEncryptedIdException();
        }
    }
}
