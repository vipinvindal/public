package com.quarks.scrabble.validator.player;

import com.quarks.scrabble.model.Player;
import com.quarks.utils.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validator class for player
 * Created by apple on 2/26/17.
 */
@Component
public class PlayerValidator implements Validator{
	
    @Override
    public boolean supports(Class<?> aClass) {
        return Player.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Player player = (Player) o;
        if(StringUtils.isNull(player.getEmail())){
            errors.rejectValue("email",null,"Email required");
        }
        if(StringUtils.isNull(player.getName())){
            errors.rejectValue("name",null,"name required");
        }
        if(StringUtils.isNull(player.getName())){
            errors.rejectValue("mobile",null,"mobile required");
        }
    }
}
