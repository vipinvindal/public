/**
 * 
 */
package com.quarks.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * This utility is for date formating  
 * @author Quarks
 * 
 */
public class DateUtils {

	/**
	 * Converts a date object to yyyy-MM-dd HH:mm:ss representation
	 * @param date
	 */
	public static final String YyyyMmDdHmsFormat = "yyyy-MM-dd HH:mm:ss";
	public static final String DdmmyyyyHHmmssFormat = "dd/MM/yyyy HH:mm:ss";
	public static final String DdmmyyyyFormat = "dd/MM/yyyy";
	public static final String MmddyyyyFormat = "MM/dd/yyyy";
	public static final String YyyyMmDd = "yyyy-MM-dd";
	public static final String YYYYMmDd = "yyyy/MM/dd";
	public static final String ddMMYYYY = "dd.MM.yyyy";
	public static final String yyyyMMdd = "yyyyMMdd";
	public static final String yyyyMMddSlashed = "yyyy/MM/dd";
	public static final String dd_mm_yyyy = "dd-MM-yyyy";
	public static final String DdMonthYearFormat = "dd MMMM,yyyy";
	public static final String DdMonthYearHHmmFormat = "dd MMMM yyyy hh:mm a";
	public static final String Day_DdMonthYearFormat = "EEE, dd MMM yyyy";
	public static final String Day_DdMonthFormat = "EEE, dd MMM";
	public static final String HOUR_MINUTE_SEC = "HH:mm:ss";
	public static final String[] MONTHS_ARRAY = {"January","February","March","April","May","June","July","August","September","October","November","December"};
	public static final String YYYYMMDDPlain = "YYYYMMdd";
	public static final String YYYYMMDD24HHmmssPlain = "yyyyMMddHHmmss";
	
	public static String convertDateToString(Date date, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}
	
	public static Date convertStringToDate(String date, String format) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.parse(date);
	}

	public static String getCurMySQLDate() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.YyyyMmDd);
		return formatter.format(date);
	}
	
	public static String getCurMySQLDateWithTime() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.YyyyMmDdHmsFormat);
		return formatter.format(date);
	}
	
	public static String getDateInyyyyMMddFormat(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DATE);
		return new StringBuilder().append(year).append(month).append(day).toString();
	}
	
	/**
	 * Return string representation of date in dd/MM/yyyy format
	 * @param date
	 */
	public static String getDateInddMMyyyyFormat(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DATE);
		return new StringBuilder().append(day).append("/").append(month).append("/").append(year).toString();
	}
	
	/**
	 * Get time in AM/PM format
	 */
	public static String getAmPmTime(int hourOfDay){
		if(hourOfDay <= 12){
			return hourOfDay + " AM";
		}
		hourOfDay = hourOfDay - 12;
		return hourOfDay + " PM";
	}
	
	/**
	 * Adds specified number of days in the date
	 * @param date
	 * @param days
	 * @return modified date
	 */
	public static Date addDays(Date date,int days){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		return calendar.getTime();
	}
	
	/**
	 * Add minutes to time
	 */
	public static Date addMinutes(Date date,int minutes){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minutes);
		return calendar.getTime();
	}
	
	/**
	 * Get Day Start
	 * @param date
	 * @return modified date
	 */
	public static Date dayStart(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	
	/**
	 * Get Day Start
	 * @param date
	 * @return modified date
	 */
	public static Date currentDayStart(){
		Date date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	
	public static Date currentDayEnd(){
		Date date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND,59);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	
	/**
	 * Get Day End
	 * @param date
	 * @return modified date
	 */
	public static Date dayEnd(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND,59);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	
	public static Date threeYearsLater(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, 3);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND,59);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	
	public static Date threeMonthsEarlier(){
		Date date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -3);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	
	/**
	 * Get Month End
	 * @param date
	 * @return modified date
	 */
	public static Date dayMonthEnd(Integer month, Integer year){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);  
	    calendar.add(Calendar.DATE, -1);  
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND,59);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	
	/**
	 * Get Month Start
	 * @param date
	 * @return modified date
	 */
	public static Date dayMonthStart(Integer month, Integer year){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.DAY_OF_MONTH, 1);  
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	

	/**
	 * Get Month Start
	 */
	public static Date dayMonthStart(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);  
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND,0);
		calendar.set(Calendar.MILLISECOND,0);
		return calendar.getTime();
	}
	/**
	 * Range of date from to both are inclusive.
	 * @param from
	 * @param to
	 * @return
	 */
	public static List<Date> getDateRange(Date from, Date to) {
		from = DateUtils.dayStart(from);
		to = DateUtils.dayStart(to);
		List<Date> dates = new ArrayList<Date>();
		if ( from.after(to)) return null;
		if ( from.equals(to)) {
			dates.add(from);
			return dates;
		}else {
			Date _temp = from;
			dates.add(from);
			while (!_temp.equals(to)) {
				_temp = addDays(_temp, 1);
				dates.add(_temp);
			}
		}
		return dates;
	}
	
	/**
	 * Utility method to calculate age
	 */
	public static int getAge(Date dateOfBirth){
		Calendar dob = Calendar.getInstance();  
		dob.setTime(dateOfBirth);  
		Calendar today = Calendar.getInstance();  
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
		  age--;  
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
		    && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
		  age--;  
		}
		return age;
	}
	
	
	public static String getCurYear(){
		Date date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		return new StringBuilder().append(year).toString();
	}
	
	public static String getCurMonth(){
		Date date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int month = calendar.get(Calendar.MONTH) + 1;
		return new StringBuilder().append(month).toString();
	}
	
	public static String getCurDate(){
		Date date = new Date(System.currentTimeMillis());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int day = calendar.get(Calendar.DATE);
		return new StringBuilder().append(day).toString();
	}
	
	public static Map<String, String> getMonthYearMap(Date date) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		Date targetDate = new Date(System.currentTimeMillis());
		Calendar cal = Calendar.getInstance();
	//	 String[] MONTHS_ARRAY = {"January","February","March","April","May","June","July","August","September","October","November","December"};
		while((dayMonthStart(targetDate)).after(dayMonthStart(date)) || (dayMonthStart(targetDate)).equals(dayMonthStart(date))) {
			cal.setTime(targetDate);
			int month = cal.get(Calendar.MONTH);
			int year = cal.get(Calendar.YEAR);
			String monthName = MONTHS_ARRAY[month];
			map.put(month + "-" +year, monthName + ", "+year);
			 // --month
			cal.add(Calendar.MONTH, -1);
			targetDate = cal.getTime();
		}
		return map;
	}
	
	

	public static int getSecondsToThreeYears() {
		Date currentDate = new Date(System.currentTimeMillis());
		Date endDate = threeYearsLater(currentDate);
		return (int) ((endDate.getTime() - currentDate.getTime())/1000);
	}
	
	public static int getTimeTillNowInMillis(Date fromDate) {
		return (int) (System.currentTimeMillis()-fromDate.getTime());
	}
	/**
	 * Converts a string date to a timestamp object
	 * @param timestamp
	 * @param format
	 * @return Timestamp
	 * @throws java.text.ParseException
	 */
	
	public static Timestamp convertStringToTimestamp(String timestamp, String format) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return new Timestamp(formatter.parse(timestamp).getTime());
	}
	
	public static int getNumberOfYearFromCurrentDate(Date date) throws ParseException {
		 Calendar cal = Calendar.getInstance();
		 Calendar currCal = Calendar.getInstance();
		 cal.setTime(date);
		 currCal.setTime(new Date(System.currentTimeMillis()));
		 int diff = currCal.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
         return diff;
	}

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
