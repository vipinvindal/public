package com.quarks.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.Key;
import java.util.StringTokenizer;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Utility class for Encryption/Decryption Salt  
 * @author Quarks
 * 
 */
public class EncryptionUtils {
    private static Cipher ecipher = null;
    private static Cipher dcipher = null;

    protected static String KEYGEN_STR = "qu@rksCoding";
    
    public static final String CARRIAGE_RETURN = "\r";
    public static final String CARRIAGE_RETURN_REPLACEMENT = "__CRGRTRN__";
    public static final String PLUS = "\\+";
    public static final String PLUS_REPLACEMENT = "__PLS__";
    public static final String SLASH = "/";
    public static final String SLASH_REPLACEMENT = "__SLSH__";
    public static final String NEWLINE = "\n";
    public static final String NEWLINE_REPLACEMENT = "__NWLIN__";

    /**
     * Get Key to encrypt/decrypt
     */
    private static Key getKey(){
        try{
            byte[] bytes = getbytes(KEYGEN_STR);
            DESKeySpec pass = new DESKeySpec(bytes); 	            
            SecretKeyFactory sKeyFactory = SecretKeyFactory.getInstance("DES"); 
            SecretKey sKey = sKeyFactory.generateSecret(pass); 
            return sKey;
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
	
    /**
     * Get bytes
     */
    private static byte[] getbytes(String str){
        ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();	        
        StringTokenizer sTokenizer = new StringTokenizer(str, "-", false);
        while(sTokenizer.hasMoreTokens()){
            try{
                byteOutputStream.write(sTokenizer.nextToken().getBytes());
            }
            catch(IOException ex){
           
            }
        }	        
        byteOutputStream.toByteArray();
        return byteOutputStream.toByteArray();
    }
    
    /**
     * Encrypting a string
     * @param sourceStr
     */
    public static String encrypt(String sourceStr){ 
    	if(sourceStr == null || "".equals(sourceStr))
    		return null;
        try{
            // Get secret key
            Key key = getKey();	             
            ecipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            ecipher.init(Cipher.ENCRYPT_MODE, key); 
            byte[] enc = ecipher.doFinal((new String(sourceStr)).getBytes("UTF-8"));
            String encryptedResult = new String(Base64.encodeBase64(enc));
            if(encryptedResult!= null ){
            	encryptedResult	=	encryptedResult.replaceAll(CARRIAGE_RETURN, CARRIAGE_RETURN_REPLACEMENT);
	    		encryptedResult	=	encryptedResult.replaceAll(PLUS, PLUS_REPLACEMENT);
		    	encryptedResult = 	encryptedResult.replaceAll(SLASH, SLASH_REPLACEMENT);
		    	encryptedResult = 	encryptedResult.replaceAll(NEWLINE, NEWLINE_REPLACEMENT);
	    	}
            return encryptedResult;
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Decrypting a encrypted string
     * @param sourceStr
     */
    public static String decrypt(String sourceStr){
    	if(sourceStr == null || "".equals(sourceStr)){
    		return null;
    	}
        try{
        	sourceStr = sourceStr.replaceAll(NEWLINE_REPLACEMENT, NEWLINE);
        	sourceStr = sourceStr.replaceAll(SLASH_REPLACEMENT, SLASH);
        	sourceStr = sourceStr.replaceAll(PLUS_REPLACEMENT, PLUS);
        	sourceStr = sourceStr.replaceAll(CARRIAGE_RETURN_REPLACEMENT, CARRIAGE_RETURN);

        	// Get secret key
            Key key = getKey();
            
            dcipher = Cipher.getInstance("DES/ECB/PKCS5Padding");                      
            dcipher.init(Cipher.DECRYPT_MODE, key);  
            
            byte[] dec = Base64.decodeBase64(sourceStr.getBytes());
            byte[] utf8 = dcipher.doFinal(dec);
            return new String(utf8, "UTF-8");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    } 
	
    //Checking encryption/decryption
	public static void main(String[] args) throws Exception {
		String st = "vipinvindal@qtsolv.com";
		String enc = encrypt(st);
		System.out.println("Encrypted string :" + enc);
		System.out.println("Decrypted string :" + decrypt(enc));
	}
}
