package com.quarks.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

/**
 * File related utilities
 * @author Quarks
 *
 */
public class FileUtils {
	
	/**
	 * Create a gzip file from a source file
	 */
	public static void creategZipFile(String outputFileAbsolutePath, String sourceFileAbsolutePath){
		byte[] buffer = new byte[1024];
		try{
	    	GZIPOutputStream gzos = new GZIPOutputStream(new FileOutputStream(outputFileAbsolutePath));
	        FileInputStream in = new FileInputStream(sourceFileAbsolutePath);
	        int len;
	        while ((len = in.read(buffer)) > 0) {
	        	gzos.write(buffer, 0, len);
	        }
	        in.close();
	    	gzos.finish();
	    	gzos.close();
	    	System.out.println("Done");
	    }catch(IOException ex){
	       ex.printStackTrace();   
	    }
	}

	
	/**
	 * Create a unique filename from different string
	 * @param originalFilename
	 * @param email
	 * @param uploadType
	 * @return
	 */
	public static String getFileName(String originalFilename, String email, String uploadType){
		String emailFirstPart = "";
		if(email != null && !"".equals(email.trim()) && email.indexOf("@") > 0){
			//split email to get the part before @
			emailFirstPart = email.split("@")[0];
		}else{
			emailFirstPart =  email;
		}
		//remove all special character from string 
		emailFirstPart = emailFirstPart.replaceAll("[^a-zA-Z0-9_-]", "");
		
		return emailFirstPart+"-"+QuarksUtils.getCurrentEpochTime()+"-"+uploadType+"."+originalFilename.split("\\.")[1];
	}
}
