package com.quarks.utils;

import java.util.Date;

/**
 * File related utilities
 * @author Quarks
 *
 */
public class QuarksUtils {

	/**
	 * Get epoch time for the current date
	 * @return
	 */
    public static long getCurrentEpochTime() {
        Date currentDate = new Date();
        long epochstring = currentDate.getTime() / 1000;
        return epochstring;
    }
}
