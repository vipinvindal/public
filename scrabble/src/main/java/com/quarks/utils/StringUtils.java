package com.quarks.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Utility class for String related operations
 * @author Quarks
 */
public class StringUtils {

	/**
	 * Checks whether a String value is empty or null
	 * @param value
	 */
	public static boolean isNull(Object value) {
		String strValue = null;
		if (value instanceof Integer || value instanceof Long || value instanceof Double
				|| value instanceof String || value instanceof Date || value instanceof Boolean) {
			strValue = value.toString();
		}
		if ((strValue == null)
				|| (strValue != null && ("".equals(strValue.trim())
						|| "null".equals(strValue.trim()) || strValue.trim()
						.length() == 0)))
			return true;
		else
			return false;
	}
	
	/**
	 * Get SQL values for comma separated values
	 */
	public static String getSqlValues(String csv){
		StringBuffer queryValuesBuffer = new StringBuffer();
		String[] values = csv.split(",");
		int length = values.length;
		int counter = 0;
		for(String value : values){
			counter ++;
			if(counter == length){
				queryValuesBuffer.append("'" + value + "'");
			}else{
				queryValuesBuffer.append("'" + value + "',");
			}
		}
		return queryValuesBuffer.toString();
	}
	
	/**
	 * Get Long List for comma separated values
	 */
	public static List<Long> getLongArray(String csv){
		List<Long> longValues = new ArrayList<Long>();
		String[] values = csv.split(",");
		for(String value : values){
			longValues.add(Long.parseLong(value));
		}
		return longValues;
	}
	
	/**
	 * Get String List for comma separated values
	 */
	public static List<String> getStringArray(String csv){
		List<String> stringValues = new ArrayList<String>();
		String[] values = csv.split(",");
		for(String value : values){
			stringValues.add(value);
		}
		return stringValues;
	}
	
	/**
	 * Method to get last word with ..
	 */
	public static String getStringTillLastWorld(String input){
		String output = input;
		if(StringUtils.isNotNull(input)){
			int lastSpacePos = input.lastIndexOf(" ");
			if(lastSpacePos > 0) {
				output = input.substring(0,lastSpacePos) + "..";
			} else {
				output = input + "..";
			}

		}
		return output;
	}
	

	/**
	 * Method to get last word with ..
	 */
	public static String getStringTillLastWorldOnly(String input){
		String output = input;
		if(StringUtils.isNotNull(input)){
			int lastSpacePos = input.lastIndexOf(" ");
			if(lastSpacePos > 0) {
				output = input.substring(0,lastSpacePos);
			} else {
				output = input;
			}

		}
		return output;
	}
	
	/**
	 * Get SEO friendly text
	 */
	public static String getSeoFriendlyText(String text){
		//Guard Clause
		if(StringUtils.isNull(text)) return text;
		text = text.trim();
		text = text.replaceAll("[/]", "-");
		text = text.replaceAll("[^A-Za-z0-9- ]", "");
		text = text.toLowerCase().replaceAll(" ", "-");
		return text;
	}
	
	/**
	 * Checks whether a String value is empty or null
	 * @param value
	 */
	public static boolean equals(Object object1,Object object2) {
		if(object1 == null || object2 == null){
			return Boolean.FALSE;
		}
		return object1.equals(object2);
	}
	
	/**
	 * Removes Last Comma If Present
	 */
	public static String removeLastComma(String value){
		if(StringUtils.isNotNull(value)){
			int index = value.lastIndexOf(",");
			value = value.substring(0, index);
		}
		return value; 
	}
	
	/**
	 * Removes Last Comma If Present
	 */
	public static String removeFirstComma(String value){
		if(StringUtils.isNotNull(value)){
			if(value.startsWith(",")){
				value = value.substring(1);
			}
		}
		return value; 
	}
	
	/**
	 * Checks whether a String value is empty or null
	 * @param value
	 */
	public static boolean isNotNull(Object value) {
		return ! isNull(value);
	}
	
	/**
	 * Cleans Email String
	 * @param email
	 * @return
	 */
	public static String cleanEmailString(String email){
		if(isNotNull(email)){
			email = email.replaceAll("[;\\s\\,]+",",");
		}
		return email;
	}
	
	/**
	 * Generate UserName
	 */
	public static String generateUserName(String name){
		if(isNotNull(name)){
			name = name.trim();
			name = name.replaceAll(" ","-");
			name = name + "-";
		}
		return name + System.currentTimeMillis();
	}
	
	/**
	 * This method splits any comma, space and arrow separated string.
	 * @param str
	 * @return array of string
	 */
	public static String[] split(String str) {
		final String REGEX = "\\s*(\\s|=>|,)\\s*";
		final Pattern p = Pattern.compile(REGEX);
        return p.split(str);
	}
	
	public static String getXMLFriendlyString(String input){
		if(StringUtils.isNotNull(input)){
			input = input.replaceAll("&(?!#?[a-z0-9]+;)", "&amp;");
		}
		return input;
	}
	
	/**
	 * Split sting with commas
	 * @param str
	 * @return
	 */
	public static String[] splitWithComma(String str) {
		final String REGEX = ",\\s*";
		final Pattern p = Pattern.compile(REGEX);
        return p.split(str);
	}
	
	/**
	 * Split sting with hyphen
	 * @param str
	 * @return
	 */
	public static String[] splitWithHyphen(String str) {
		final String REGEX = "-\\s*";
		final Pattern p = Pattern.compile(REGEX);
        return p.split(str);
	}
	
	/**
	 * Split String with some predefined pattern
	 * @param str
	 * @param pattern
	 * @return
	 */
	public static String[] splitWithPattern(String str, String pattern) {
		final String REGEX = pattern+"\\s*";
		final Pattern p = Pattern.compile(REGEX);
        return p.split(str);
	}
	

	/**
	 * Split String searchQuery
	 * @param str
	 * @param pattern
	 * @return
	 */
	public static String[] parsQuerySearch(String searchQuery, String pattern) {
		final String REGEX = pattern;
		final Pattern p = Pattern.compile(REGEX);
        return p.split(searchQuery);
	}
	
	public static void main(String[] args) {
		
	}

    public static boolean isStringEmpty(String str){
        return isNull(str);
    }

}
