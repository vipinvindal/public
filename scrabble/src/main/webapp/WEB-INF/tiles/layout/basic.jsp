<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html>
    <head>
        <title><tiles:getAsString name="title"/></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="/static/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="/static/css/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <!-- Header -->
        <div class="container-narrow">
            <tiles:insertAttribute name="header"/>
            <!-- Body -->
            <tiles:insertAttribute name="body"/>
            <!-- Footer -->
            <tiles:insertAttribute name="footer"/>
         </div>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="/static/js/bootstrap.min.js"></script>
    </body>
</html>
