<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="page-header">
    <h1>Leader Board</h1>
</div>
<table class="table table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th></th>
        <th>Name</th>
        <th>Highest score</th>
        <th>Average score</th>
        <th>Match played</th>
        <th>Wins</th>
        <th>Loses</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${leaderBoard}" var="playerStats" varStatus="status">
        <tr>
            <td>${(status.index + 1)}</td>
            <td>
                <c:choose>
                    <c:when test="${empty playerStats.player.profilePicture}">
                        <img src="${playerStats.player.profilePicture.encryptedImageUrl}" class="img-circle">
                    </c:when>
                    <c:otherwise>
                        <img src="http://placehold.it/350x150" class="img-circle">
                    </c:otherwise>
                </c:choose>
            </td>
            <td>
                <a href="/stats/${playerStats.player.encryptedId}/">${playerStats.player.name}</a>
            </td>
            <td>
                <c:choose>
                    <c:when test="${playerStats.highestScoreMatch.player1.id == playerStats.player.id}">
                        ${playerStats.highestScoreMatch.player1Score}
                    </c:when>
                    <c:otherwise>
                        ${playerStats.highestScoreMatch.player2Score}
                    </c:otherwise>
                </c:choose>
            </td>
            <td><fmt:formatNumber type="number"
                                  maxFractionDigits="2" value="${playerStats.averageScore}" />
            </td>
            <td>${playerStats.noOfMatches}</td>
            <td>${playerStats.noOfWins}</td>
            <td>${playerStats.noOfLoses}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $("ul.nav").find("li.active").removeClass("active");
        $("ul.nav > li:nth-child(3)").addClass("active");
    });
</script>