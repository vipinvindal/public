<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="page-header">
    <h2>${playerStats.player.name}</h2>
</div>
<table class="table table-striped">
    <tbody>
    <tr>
        <td>Average score</td>
        <td colspan="2"><fmt:formatNumber type="number"
                              maxFractionDigits="2" value="${playerStats.averageScore}" /></td>
    </tr>
    <tr>
        <td>Highest score</td>
        <td>
            <c:choose>
                <c:when test="${playerStats.highestScoreMatch.player1.id == playerStats.player.id}">
                    ${playerStats.highestScoreMatch.player1Score}
                    <c:set var="against" value="${playerStats.highestScoreMatch.player2}"/>

                </c:when>
                <c:otherwise>
                    ${playerStats.highestScoreMatch.player2Score}
                    <c:set var="against" value="${playerStats.highestScoreMatch.player1}"/>
                </c:otherwise>
            </c:choose>
        </td>
        <td>
            <a class="pull-right" href="/stats/${against.encryptedId}/">
                ${against.name}
            </a>
        </td>
    </tr>
    <tr>
        <td>Match played</td>
        <td colspan="2">${playerStats.noOfMatches}</td>
    </tr>
    <tr>
        <td>Wins</td>
        <td colspan="2">${playerStats.noOfWins}</td>
    </tr>
    <tr>
        <td>Loses</td>
        <td colspan="2">${playerStats.noOfLoses}</td>
    </tr>
    </tbody>
</table>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $("ul.nav").find("li.active").removeClass("active");
        $("ul.nav > li:nth-child(2)").addClass("active");
    });
</script>