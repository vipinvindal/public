<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="page-header">
    <h1>Profile</h1>
</div>
<c:if test="${message != null}">
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        ${message}
    </div>
</c:if>
<form:form class="form-horizontal" commandName="player" method="post">
    <div class="control-group">
        <label class="control-label" for="inputName">Name</label>
        <div class="controls">
            <form:input type="text" required="" maxlength="100" id="inputName" path="name" placeholder="Name"/>
            <form:errors path="name" cssClass="help-inline error" element="span" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputMobile">Mobile</label>
        <div class="controls">
            <form:input type="text" required="" maxlength="20" path="mobile" id="inputMobile" placeholder="Mobile"/>
            <form:errors path="mobile" cssClass="help-inline error" element="span" />
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Update</button>
        </div>
    </div>
</form:form>