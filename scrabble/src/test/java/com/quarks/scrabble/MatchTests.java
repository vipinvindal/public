package com.quarks.scrabble;

import com.quarks.scrabble.model.Match;
import com.quarks.scrabble.model.Player;
import com.quarks.scrabble.service.MatchService;
import com.quarks.scrabble.service.PlayerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by apple on 2/26/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MatchTests {

    private static final Long PLAYER_1_ID = 2L;
    private static final Long PLAYER_2_ID = 3L;
    private static final Long PLAYER_1_SCORE = 350L;
    private static final Long PLAYER_2_SCORE = 250L;
    private static final Long MATCH_ID = 2L;

    @Autowired
    MatchService matchService;

    @Autowired
    PlayerService playerService;

    @Test
    @Transactional
    public void createNewMatch(){
        Match match = new Match();
        Player player1 = playerService.findById(PLAYER_1_ID);
        Player player2 = playerService.findById(PLAYER_2_ID);
        match.setPlayer1(player1);
        match.setPlayer2(player2);
        match.setPlayer1Score(PLAYER_1_SCORE);
        match.setPlayer2Score(PLAYER_2_SCORE);
        match = matchService.saveOrUpdate(match);
        assertThat(match.getId()).isNotNull();
    }

    @Test
    public void getMatchById(){
        Match match = matchService.findById(MATCH_ID);
        assertThat(match).isNotNull();
    }
}
