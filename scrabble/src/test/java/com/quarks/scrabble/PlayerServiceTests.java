package com.quarks.scrabble;

import com.quarks.exception.InValidEncryptedIdException;
import com.quarks.scrabble.model.Player;
import com.quarks.scrabble.service.PlayerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by apple on 2/26/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayerServiceTests {

    private static final Long TEST_PLAYER_ID = 2L;
    private static final String TEST_PLAYER_ENCRYPTED_ID = "encriptedID";

    @Autowired
    PlayerService playerService;

    @Test
    public void contextLoads() {
    }

    @Test
    public void getPlayerById() {
        Player player = playerService.findById(TEST_PLAYER_ID);
        assertThat(player).isNotNull();
    }

    @Test
    public void getPlayerByIdEncrypted() {
        try{
            playerService.getPlayer(TEST_PLAYER_ENCRYPTED_ID);
        }catch (Exception e){
            assertThat(e.getClass()).isEqualTo(InValidEncryptedIdException.class);
        }
    }
    @Test
    @Transactional
    public void createPlayer(){
        Player player = new Player();
        player.setEmail("testEmail@test.com");
        player.setName("TestName");
        player.setMobile("9811815473");
        player.setPasswordHash("123456789");
        player = playerService.saveOrUpdate(player);
        assertThat(player.getId()).isNotNull();
        player.setName("UpdatedTestName");
        player = playerService.saveOrUpdate(player);
        assertThat(player.getName().toString()).isEqualTo("UpdatedTestName");
    }

}
