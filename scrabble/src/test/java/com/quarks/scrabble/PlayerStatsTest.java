package com.quarks.scrabble;

import com.quarks.scrabble.model.PlayerStats;
import com.quarks.scrabble.service.PlayerStatsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by apple on 2/26/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayerStatsTest {

    private static final Long PLAYER_1_ID = 2L;

    @Autowired
    PlayerStatsService playerStatsService;


    @Test
    public void getPlayerStats(){
        PlayerStats playerStats = playerStatsService.getPlayerStats(PLAYER_1_ID);
        assertThat(playerStats).isNotNull();
        assertThat(playerStats.getPlayer().getId()).isEqualTo(PLAYER_1_ID);
    }
    @Test
    @Transactional
    public void savePlayerStats(){
        PlayerStats playerStats = playerStatsService.getPlayerStats(PLAYER_1_ID);
        playerStats.setNoOfMatches(10L);
        playerStats = playerStatsService.saveOrUpdate(playerStats);
        assertThat(playerStats.getNoOfMatches()).isEqualTo(10L);
    }
}
